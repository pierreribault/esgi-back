<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\TodoListRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController
{

    /**
     * @var TodoListRepository
     */
    private $todoListRepository;

    public function __construct(TodoListRepository $todoListRepository)
    {
        $this->todoListRepository = $todoListRepository;
    }

    /**
     * @Route("/api", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        $items = $this->todoListRepository->findAll();

        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();

        $serializer = new Serializer([$normalizer], [$encoder]);

        $response = $serializer->serialize($items, 'json');

        return new JsonResponse(
            $response,
            200,
            ['Access-Control-Allow-Origin' => '*']
        );
    }

    /**
     * @Route("/api/add", name="add", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = $request->request->all();

        dump($data);

        $id = $data['id'];
        $title = $data['title'];
        $completed = $data['completed'];

        if (empty($id) || empty($title) || empty($completed)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $this->todoListRepository->save($id, $title, $completed);

        return new JsonResponse(['status' => 'todolist created!'], Response::HTTP_CREATED, ['Access-Control-Allow-Origin' => '*']);
    }

    // passer un header qui ouvre le cors a tout le monde avec une *
    // sur le front mettre un reverse proxy ou le configurer

    /**
     * @Route("/api/update", name="update", methods={"POST"})
     */
    public function update(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = $request->request->all();

        $id = $data['id'];
        $title = $data['title'];
        $completed = $data['completed'];

        $todolist = $this->todoListRepository->findOneBy(['id' => $id]);
        $todolist->setTitle($title);
        $todolist->setCompleted($completed);

        $entityManager->persist($todolist);
        $entityManager->flush();


        return new JsonResponse(['status' => 'todolist updated!'], Response::HTTP_CREATED, ['Access-Control-Allow-Origin' => '*']);
    }

    /**
     * @Route("/api/delete", name="delete", methods={"POST"})
     */
    public function delete(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = $request->request->all();

        $id = $data['id'];

        $todolist = $this->todoListRepository->findOneBy(['id' => $id]);
        $entityManager->remove($todolist);
        $entityManager->flush();

        return new JsonResponse(['status' => 'todolist deleted!'], Response::HTTP_CREATED);
    }

}

